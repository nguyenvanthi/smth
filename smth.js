const express = require('express');
const packages = require('./package');

// Variable
const app = express();

// Configure
require('./config/init')(app, packages, __dirname);
require('./config/structure')(app);
require('./config/files')(app);
require('./config/express')(app);

app.loadUtils(app); // Load all utils
app.loadFeatures(app); // Load all features
// Launch website
app.startServer();
