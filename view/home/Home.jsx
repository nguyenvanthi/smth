import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { connect, Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import client from 'axios';
import { SnackbarProvider, withSnackbar } from 'notistack';

import { createMuiTheme, MuiThemeProvider, withStyles } from '@material-ui/core/styles';

// Components
import Router from './Router.jsx';

// Redux
import firebase, { initFirebase } from '../redux/firebase.jsx';
import food from '../redux/food.jsx';
import order, {getAllOrder} from '../redux/order.jsx';
import charge from '../redux/charge.jsx';
import user, { getAllUser } from '../redux/user.jsx';

// Libraries
require('../utils/number');

// Axios
const thunkWithClient = thunk.withExtraArgument(client);

// Init redux
const allReducers = combineReducers({ firebase, food, order, user, charge });
const store = createStore(allReducers,
  {},
  composeWithDevTools(applyMiddleware(thunkWithClient))
);
store.dispatch(initFirebase());
store.dispatch(getAllUser());

store.getState().firebase.database().ref().on('value', () => {
  store.dispatch(getAllOrder());
})

const theme = createMuiTheme({
  palette: {
    primary: { main: '#673ab7' },
    secondary: { main: '#ede7f6' },
  },
  typography: { useNextVariants: true },
});

/**
 * Main component
 */
class App extends React.Component {
  /**
   * Render component
   * @return {Html} Html content
   */
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <Router />
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state) => state;
const mapActionsToProps = {};
const Main = withStyles(theme)(withSnackbar(connect(mapStateToProps, mapActionsToProps)(App)));
ReactDOM.render(
  <Provider store={store}>
    <SnackbarProvider>
      <Main />
    </SnackbarProvider>
  </Provider>,
  document.getElementById('app')
);
