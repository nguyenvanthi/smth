import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import { withSnackbar } from 'notistack';
import {
  AppBar, Toolbar, Typography, Button, Hidden, Avatar,
  Dialog, DialogTitle, DialogContent, TextField, DialogActions
} from '@material-ui/core';

import {login} from '../redux/user.jsx';

const styles = theme => ({
  grow: {
    flexGrow: 1,
  },
});

class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false, user: {username: null, password: null}};

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  show() {
    this.setState({open: true, food: {username: '', password: ''}});
  }

  hide() {
    this.setState({open: false});
  }

  handleChange = (key, value) => {
    let user = this.state.user;
    user[key] = value;
    this.setState(Object.assign({}, this.state, {user: user}));
  }

  login() {
    const user = this.state.user;
    this.props.login(user, result => {
      if (result) this.hide();
      else {
        //TODO notify
        this.props.snackbar('Tên đăng nhập hoặc mật khẩu sai!', {variant: 'warning'});
      }
    })
  }

  render() {
    return (
      <Dialog
        open={this.state.open}
        onClose={() => this.hide()}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Đăng nhập</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="usernme"
            value={this.state.user.username}
            onChange={(e) => this.handleChange('username', e.target.value)}
            label="Tên đăng nhập"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="password"
            value={this.state.user.password}
            onChange={(e) => this.handleChange('password', e.target.value)}
            label="Mật khẩu"
            type="password"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.hide} color="primary">
            Thoát
          </Button>
          <Button onClick={this.login} color="primary">
            Đăng nhập
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.signin = React.createRef();
    this.login = this.login.bind(this);
  }

  login(user, done) {
    this.props.login(user.username, user.password)
      .then(() => done(true))
      .catch(() => done(false));
  }

  render() {
    const classes = this.props.classes;
    const getStyle = (ref) => {
      return this.props.location.pathname === ref
        ? {variant:'contained', color:'primary'}
        : {variant:'text', color:'inherit'}
    }

    const foodButton = this.props.logined ? (<Button {...getStyle('/monan')} component={Link} to='/monan'>Món ăn</Button>)
      : (<Button onClick={() => this.signin.current.show()}>Đăng nhập</Button>);
    return (
      <AppBar position='static' color='inherit'>
        <Toolbar>
          <Avatar alt='' src='/image/favicon.ico' component={Link} to='/bep'/>
          <Hidden only={['xs']}>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Quản lí nhà hàng
            </Typography>
          </Hidden>
          <Button {...getStyle('/quanli')} component={Link} to='/quanli'>
            Quản lí
          </Button>
          <Button {...getStyle('/bep')} component={Link} to='/bep'>
            Bếp
          </Button>
          {foodButton}
          <Signin login={this.login} ref={this.signin} snackbar={this.props.enqueueSnackbar}/>
        </Toolbar>
      </AppBar>
    );
  }
}

const stateToProps = (state) => ({logined: state.user.logined});
const actionsToProps = {login};
export default withStyles(styles)(
    withSnackbar(connect(stateToProps, actionsToProps)(Header)));
