import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { getAllOrder, updateCooked } from '../redux/order.jsx';
import { getFoodName } from '../redux/food.jsx';
import Header from './Header.jsx';

import {
  Grid, ListItem, Paper, ListItemText,
  MobileStepper, Button, Typography, Tab, Tabs, TabContainer,
  Badge
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RestoreIcon from '@material-ui/icons/Restore';


class ProgressMobileStepper extends React.Component {
  state = {
    activeStep: 0,
    steps: 0,
    name: '',
  };

  static getDerivedStateFromProps(props, state) {
    state.activeStep = props.activeStep;
    state.steps = props.steps + 1;
    return state;
  }

  componentDidMount() {
    this.props.getName(this.props.id, this.props.type)
      .then((name) => {
        this.setState({
          activeStep: this.props.activeStep,
          steps: this.props.steps + 1,
          name: name
        });
      })
  }

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
    this.props.update(this.props.id, this.state.activeStep + 1, this.props.table);
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
    this.props.update(this.props.id, this.state.activeStep - 1, this.props.table);
  };

  render() {
    return (
      <Paper disabled={this.state.activeStep === this.state.steps - 1}>
        <Typography align='center' variant='h6'>{this.state.name + ' - ' + this.props.table}</Typography>
        <MobileStepper
          variant="progress"
          steps={this.state.steps}
          position="static"
          activeStep={this.state.activeStep}
          nextButton={
            <Button size="small" onClick={this.handleNext} disabled={this.state.activeStep === this.state.steps - 1}>
              <AddIcon />
            </Button>
          }
          backButton={
            <Button size="small" onClick={this.handleBack} disabled={this.state.activeStep === 0}>
              <RestoreIcon />
            </Button>
          }
        />
        <Typography align='center'>{this.state.activeStep} / {this.state.steps - 1} </Typography>
      </Paper>

    );
  }
}

const styles = theme => ({
  grid: {
    padding: theme.spacing.unit * 2,
  },
  margin: {
    margin: theme.spacing.unit * 2,
    padding: theme.spacing.unit
  }
});

class Ordered extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tab: 0 };
    this.renderTable = this.renderTable.bind(this);
  }

  componentWillMount() {
    this.props.getAllOrder();
  }

  renderTable(orders, classes, tableName, idx) {
    const items = orders.length > 0 ? orders.map((order, index) => {
      return (
        order.cooked != order.quantity ?
          <Grid item xs={12} sm={6} md={3} lg={4} key={index}>
            <ProgressMobileStepper
              activeStep={order.cooked}
              steps={order.quantity}
              id={order.id}
              type={order.type}
              table={tableName}
              getName={this.props.getFoodName}
              update={this.props.updateCooked} />
          </Grid>
          : <p key={index} />
      );
    }) : (<ListItem button><ListItemText primary="Danh sách đặt hàng trống!" /></ListItem>);
    return (
      <Grid container spacing={16} className={classes.grid} key={idx}>
        {items}
      </Grid>
    );
  }

  handleChange = (event, newValue) => {
    this.setState({ tab: newValue });
    event.preventDefault();
  }

  render() {
    const classes = this.props.classes;
    const orders = this.props.order && this.props.order.list
      ? this.props.order.list
      : [];
    console.log(orders);
    const items = orders.length > 0 ? orders.map((order, index) => {
      return (
        order.cooked != order.quantity ?
          <Grid item xs={12} sm={6} md={3} lg={4} key={index} className={classes.grid}>
            <ProgressMobileStepper
              activeStep={order.cooked}
              steps={order.quantity}
              id={order.id}
              type={order.type}
              table={order.table}
              getName={this.props.getFoodName}
              update={this.props.updateCooked} />
          </Grid>
          : <p key={index} />
      );
    }) : (<ListItem button><ListItemText primary="Danh sách đặt hàng trống!" /></ListItem>);
    return (
      <div>
        <Header location={this.props.location} />
        <br />
        <Grid container>
        {items}
        </Grid>
      </div>
    );

    // render() {
    //   const classes = this.props.classes;
    //   const tables = this.props.order && this.props.order.list
    //     ? this.props.order.list
    //     : [];
    //   console.log(tables);
    //   return (
    //     <div>
    //       <Header location={this.props.location} />
    //       <br />
    //       <Tabs
    //         value={this.state.tab}
    //         onChange={this.handleChange}
    //         indicatorColor="primary"
    //         textColor="primary"
    //         centered
    //       >
    //         {tables.map((table, idx) => {
    //           return (
    //             <Tab label={
    //               <Badge badgeContent={table.cookValue} color="primary" className={classes.margin}>
    //                 {table.table}
    //               </Badge>
    //             } key={idx}/>
    //           )
    //         })}
    //       </Tabs>
    //       {tables.map((table, idx) => {
    //         return idx == this.state.tab && this.renderTable(table.order, classes, table.table, idx)
    //       })}
    //     </div>
    //   );
  }
}

const stateToProps = (state) => ({ order: state.order });
const actionsToProps = { getAllOrder, updateCooked, getFoodName };
export default withStyles(styles)(
  connect(stateToProps, actionsToProps)(Ordered));
