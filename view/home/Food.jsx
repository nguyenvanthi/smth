import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';

import Header from './Header.jsx';
import {List, ListItem, ListItemIcon, ListItemText, Chip, Tooltip, Fab, Collapse, Divider, Typography,
  Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import {getAllFood, addFood, removeFood} from '../redux/food.jsx';

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2
  },
  chip: {
    marginRight: theme.spacing.unit,
  },
  space: {
    marginTop: theme.spacing.unit * 2
  }
});

class AddFoodDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false, food: {id: null, name: null, price: null, type: null}};

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.save = this.save.bind(this);
    this.handleChangeFood = this.handleChangeFood.bind(this);
  }

  show() {
    this.setState({open: true, food: {id: null, name: null, price: null, type: null}});
  }

  hide() {
    this.setState({open: false});
  }

  handleChangeFood = (key, value) => {
    let food = this.state.food;
    food[key] = value;
    if (key === 'price') food[key] = parseInt(value);
    if (key === 'id') food[key] = value.trim();
    this.setState(Object.assign({}, this.state, {food: food}));
  }

  save() {
    const food = this.state.food;
    this.props.add(food.id, food.name, food.price, food.type);
    this.hide();
  }

  render() {
    return (
      <Dialog
        open={this.state.open}
        onClose={() => this.hide()}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Thêm món ăn</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="id"
            value={this.state.food.type}
            onChange={(e) => this.handleChangeFood('type', e.target.value)}
            label="Loại món ăn"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="id"
            value={this.state.food.id}
            onChange={(e) => this.handleChangeFood('id', e.target.value)}
            label="Mã món ăn"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="name"
            value={this.state.food.name}
            onChange={(e) => this.handleChangeFood('name', e.target.value)}
            label="Tên món ăn"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="price"
            value={this.state.food.price}
            onChange={(e) => this.handleChangeFood('price', e.target.value)}
            label="Giá"
            type="number"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.hide} color="primary">
            Thoát
          </Button>
          <Button onClick={this.save} color="primary">
            Thêm
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}


class Food extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
    this.dialog = React.createRef();

    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.removeFood = this.removeFood.bind(this);
    this.showDialog = this.showDialog.bind(this);
    this.addFood = this.addFood.bind(this);
  }

  componentWillMount() {
    this.props.getAllFood();
  }

  addFood(id, name, price, type) {
    this.props.addFood(id, name, price, type);
  }

  removeFood = (id, type) => () => {
    this.props.removeFood(id, type);
  }

  toggleDrawer() {
    this.setState({ open: !this.state.open });
  }

  showDialog() {
    this.dialog.current.show();
  }

  render() {
    const foods = this.props.food && this.props.food.list
      ? this.props.food.list
      : {};
    const classes = this.props.classes;
    const list = Object.keys(foods).map((type, index) => {
      const items = foods[type].length > 0 ? foods[type].map((food, index) => {
        food.open = false;
        return (
          <div key={index}>
            <Tooltip title={food.name + ' có giá ' + food.price.format()}>
              <ListItem button onClick={this.toggleDrawer}>
                  <ListItemText primary={food.name} />
                  <ListItemIcon>
                    <Chip label={food.price.format()} color={index % 2 == 0 ? 'default' : 'primary'} className={classes.chip}/>
                  </ListItemIcon>

                  <Collapse in={this.state.open}>
                    <DeleteIcon color='error' onClick={this.removeFood(food.id, type)}/>
                  </Collapse>
              </ListItem>
            </Tooltip>
          </div>
        );
      }) : (<ListItem button><ListItemText primary="Danh sách món ăn trống!" /></ListItem>);

      return (
        <div key={index} className={{marginTop: '20px'}}>
          <Typography gutterBottom variant="h5" color='primary'>
            {type}
          </Typography>
          <Divider />
          <List component="nav">
            {items}
          </List>
        </div>
      );
    });
    return (
      <div>
        <Header location={this.props.location}/>
        <div className={classes.space}/>
        {list}
        <AddFoodDialog ref={this.dialog} add={this.addFood}/>
        <Fab className={classes.fab} color='primary' onClick={this.showDialog}>
          <AddIcon/>
        </Fab>
      </div>
    );
  }
}

const stateToProps = (state) => ({food: state.food});
const actionsToProps = {getAllFood, addFood, removeFood};
export default withStyles(styles)(
    connect(stateToProps, actionsToProps)(Food));
