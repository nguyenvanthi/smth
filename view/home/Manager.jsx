import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Header from './Header.jsx';
import { getAllCharge, paid, updateCharge } from '../redux/charge.jsx';

import {
  Grid, ListItem, Paper, ListItemText,
  Typography, List, ListItemIcon,
  Divider
} from '@material-ui/core';

import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import PaymentIcon from '@material-ui/icons/Payment';

const styles = {
};

class Manager extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getAllCharge();
  }

  render() {
    const classes = this.props.classes;
    const charge = this.props.charge;
    const tables = charge.table ? charge.table : [];
    console.log(tables);
    const items = tables.map((table, index) => {
      return (
        <Grid item xs={12} sm={6} md={3} lg={4} key={index}>
          <Paper>
            <Typography align='center' variant='h6'>{table.table}</Typography>
            <Divider />
            <List component="nav" aria-label="Main mailbox folders">
              <ListItem button>
                <ListItemIcon>
                  <ThreeSixtyIcon />
                </ListItemIcon>
                <ListItemText primary={!table.charge && table.needCook != 0 ? 'Đang nấu (' + table.needCook + ' phần)'  : 'Đã nấu xong'} />
              </ListItem>
              {table.charge && (
                <ListItem button onClick={() => this.props.paid(table.table)}>
                  <ListItemIcon>
                    <PaymentIcon />
                  </ListItemIcon>
                  <ListItemText primary={'Số tiền : ' + table.cost} />
                </ListItem>
              )}
            </List>
          </Paper>
        </Grid>
      )
    })
    return (
      <div>
        <Header location={this.props.location} />
        <br />
        <Grid container spacing={16} className={classes.grid}>
          {items}
        </Grid>
      </div>
    );
  }
}

const stateToProps = (state) => ({ charge: state.charge });
const actionsToProps = { getAllCharge, paid, updateCharge };
export default withStyles(styles)(
  connect(stateToProps, actionsToProps)(Manager));
