import React from 'react';
import { connect } from 'react-redux';
import routes from '../../resources/routes/home.json';
import ReactLoading from 'react-loading';
import Loadable from 'react-loadable';
import { Switch, Route, withRouter } from 'react-router-dom';
import { reset } from '../redux/order.jsx';

const Loading = () => <ReactLoading type='bubbles' color='#000' />;

/**
 * Router
 */
class Router extends React.Component {
  /**
   * Constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    if (props.match.path === '/reset') {
      this.props.reset(() => {
        props.history.push('/bep');
      })
    }
    if (props.match.path === '/') props.history.push('/bep');
    this.routeData = routes.map((route) => {
      return ({
        path: route.link,
        // eslint-disable-next-line new-cap
        component: Loadable({
          loading: Loading,
          loader: () => import('./' + route.component),
        }),
      });
    });
    this.routes = this.routeData.map((r, i) => <Route key={i} {...r} />);
  }

  /**
   * @return {HTML}
   */
  render() {
    return (
      <Switch>
        {this.routes}
      </Switch>
    );
  }
}

const mapStateToProps = (state) => ({});
const mapActionsToProps = { reset };
export default withRouter(connect(mapStateToProps, mapActionsToProps)(Router));
