// TYPE
const GET_ALL = 'user:getall';
const LOGIN = 'user:login';

function userReducer(state = {logined: false}, action) {
  switch (action.type) {
    case GET_ALL:
      return Object.assign({}, state, {list: action.users});
    case LOGIN:
      return Object.assign({}, state, {logined: action.result});
    default:
      return state;
  }
}

function getAllUser() {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    getState().firebase.database().ref('/User').on('value', (snapshot) => {
      const users = snapshot.val();
      dispatch({type: GET_ALL, users});
      resolve(users);
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({error});
    });
  });
}

function login(username, password) {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    const userState = getState().user;
    if (userState && userState.list) {
      if (userState.list[username] && userState.list[username] === password) {
        dispatch({type: LOGIN, result: true});
        resolve();
      } else {
        dispatch({type: LOGIN, result: false});
        reject();
      }
    } else {
      dispatch({type: LOGIN, result: false});
      reject();
    }
  });
}

export default userReducer;
export {getAllUser, login};

