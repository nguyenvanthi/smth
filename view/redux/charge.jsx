import { getFoodCost } from './food.jsx';
import { getAllOrder } from './order.jsx';
// TYPE
const GET_ALL = 'charge:getall';

function chargeReducer(state = {}, action) {
  const crtState = Object.assign({}, state);
  switch (action.type) {
    case GET_ALL:
      return { table: action.table };
    default:
      return state;
  }
}

export function getAllCharge() {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    //getState().firebase.database().ref('/Charge/ban2').set(true);
    getState().firebase.database().ref('/').on('value', (snapshot) => {
      const FB = snapshot.val();
      const orderFB = FB.Order;
      const chargeFB = FB.Charge;
      const foodFB = FB.Food;
      !orderFB && (orderFB = {});
      const tables = Object.keys(orderFB).map(table => {
        const tbData = orderFB[table];
        const orders = Object.keys(tbData).map(id => {
          const price = foodFB[tbData[id].type][id].price;
          return Object.assign({}, {id, cost: tbData[id].cooked * price}, tbData[id]);
        });
        const cost = orders.reduce((c, crt) => c + crt.cost, 0);
        const needCook = orders.reduce((c, crt) => c + crt.quantity - crt.cooked, 0);
        return {table, orders, charge: chargeFB[table], cost, needCook};
      });
      dispatch({type: GET_ALL, table: tables});
      resolve(tables);
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({ error });
    });
  });
}

export function paid(table) {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    getState().firebase.database().ref('/Order/' + table).set({}).then(() =>
      getState().firebase.database().ref('/Charge/' + table).set(false).then(() => {
        dispatch(getAllCharge())
      })
    )
  });
}

export function updateCharge(table) {
  return (dispatch, getState) => {
    getState().firebase.database().ref('/Charge/' + table).set(true).then(()=> dispatch(getAllCharge()))
  }
}

export default chargeReducer;

