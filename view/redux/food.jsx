// TYPE
const GET_ALL = 'food:getall';

function foodReducer(state = null, action) {
  switch (action.type) {
    case GET_ALL:
      return { list: action.foods };
    default:
      return state;
  }
}

function getAllFood() {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    getState().firebase.database().ref('/Food').on('value', (snapshot) => {
      const foodsFB = snapshot.val();
      const foods = {}
      Object.keys(foodsFB).forEach((type) => {
        const foodsInType = foodsFB[type];
        foods[type] = Object.keys(foodsInType).map((id) => {
          return { id: id, ...foodsInType[id] };
        });
      });
      dispatch({ type: GET_ALL, foods });
      resolve({ foods });
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({ error });
    });
  });
}

function addFood(id, name, price, type) {
  return (dispatch, getState) => new Promise((resolve) => {
    const newFood = getState().firebase.database().ref('/Food/' + type + '/' + id);
    newFood.set({ name, price });
    // dispatch(getAllFood());
    resolve();
  });
}

function removeFood(id, type) {
  return (dispatch, getState) => new Promise((resolve) => {
    const newFood = getState().firebase.database().ref('/Food/' + type + '/' + id);
    newFood.remove();
    // dispatch(getAllFood());
    resolve();
  });
}

function getFoodName(id, type) {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    getState().firebase.database().ref('/Food/' + type + '/' + id + '/name').once('value', (snapshot) => {
      const name = snapshot.val();
      resolve(name);
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({ error });
    });
  });
}

export function getFoodCost(id, type, firebase) {
  return new Promise((resolve, reject) => {
    firebase.database().ref('/Food/' + type + '/' + id + '/price').once('value', (snapshot) => {
      resolve(snapshot.val());
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({ error });
    });
  });
}

export default foodReducer;
export { getAllFood, addFood, removeFood, getFoodName };

