/* eslint-disable quotes */
/* eslint-disable require-jsdoc */
import firebase from 'firebase';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyD9YmPH4MFwMF03_4IgPtE3Uf8itt81k-c",
  databaseURL: "https://demofirebase-b0905.firebaseio.com/",
};
// TYPE
const INIT_FIREBASE = 'firebase:init';

function firebaseReducer(state = null, action) {
  switch (action.type) {
    case INIT_FIREBASE:
      // eslint-disable-next-line no-case-declarations
      const fb = firebase.initializeApp(config);
      const order = fb.database().ref();
      order.on("value",
          function(snapshot) {
            console.log(snapshot.val());
          },
          function (errorObject) {
            console.log("The read failed: " + errorObject.code);
          });
      return fb;
    default:
      return state;
  }
}

function initFirebase() {
  return (dispatch) => {
    dispatch({type: INIT_FIREBASE});
    return Promise.resolve();
  };
}

export default firebaseReducer;
export {initFirebase};

