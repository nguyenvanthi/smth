import { updateCharges } from './charge.jsx';
import { get } from 'https';

// TYPE
const GET_ALL = 'order:getall';

function orderReducer(state = null, action) {
  switch (action.type) {
    case GET_ALL:
      return Object.assign({}, state, { list: action.orders });
    default:
      return state;
  }
}

function getAllOrder() {
  return (dispatch, getState) => new Promise((resolve, reject) => {
    getState().firebase.database().ref('/Order').once('value', (snapshot) => {
      let orderFB = snapshot.val();
      !orderFB && (orderFB = {});
      const orders = Object.keys(orderFB).map(table => {
        return Object.keys(orderFB[table])
          .map(id => ({ id, table, ...orderFB[table][id] }));
      }).reduce((result, o) => {
        result.push(...o);
        return result;
      }, []).sort(function(a, b){
        return a.time - b.time
      });
      // const orders = Object.keys(orderFB).map((id) => {
      //   const cookValue = Object.keys(orderFB[id]).reduce((count, _id) => {
      //     return count + orderFB[id][_id].quantity - orderFB[id][_id].cooked;
      //   }, 0);
      //   return {
      //     table: id,
      //     order: Object.keys(orderFB[id]).map(_id => ({ id: _id, ...orderFB[id][_id] })),
      //     cookValue
      //   }
      // });
      dispatch({ type: GET_ALL, orders });
      resolve(orders);
    }, (error) => {
      console.log("The read failed: " + error.code);
      reject({ error });
    });
  });
}

export function reset() {
  const order = {
    ban1: {
      muc5: {
        type: 'SQUID',
        cooked: 0,
        quantity: 4,
        time: 1562299693
      },
      muc6: {
        type: 'SQUID',
        cooked: 0,
        quantity: 8,
        time: 1562299894
      },
      nuoc2: {
        cooked: 0,
        quantity: 4,
        type: 'DRINK',
        time: 1562299795
      }
    },
    ban2: {
      muc5: {
        type: 'SQUID',
        cooked: 0,
        quantity: 10,
        time: 1562299699
      },
      muc6: {
        type: 'SQUID',
        cooked: 0,
        quantity: 8,
        time: 1562299690
      },
    }
  }

  return (dispatch, getState) =>
    getState().firebase.database().ref('/Order').set(order).then(() =>
      getState().firebase.database().ref('/Charge').set({ ban1: false, ban2: false })
        .then(() => dispatch(getAllOrder()))
    )
}

function updateCooked(foodId, cooked, table) {
  return (dispatch, getState) => new Promise(() => {
    const cookedFB = getState().firebase.database().ref('/Order/' + table + '/' + foodId + '/' + '/cooked');
    cookedFB.set(parseInt(cooked))
      .then(() => {
        dispatch(getAllOrder());
      })
  });
}

export default orderReducer;
export { getAllOrder, updateCooked };

