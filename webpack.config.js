/* eslint-disable no-console */
const packages = require('./package');
const mm = require('micromatch');
const fs = require('fs');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');

// Create bundle folder if not exist
if (!fs.existsSync(path.join(__dirname, 'public', 'bundle'))) {
  fs.mkdirSync(path.join(__dirname, 'public', 'bundle'));
}

// eslint-disable-next-line no-extend-native
Array.prototype.isArray = true;
// eslint-disable-next-line no-extend-native
String.prototype.upFirstChar = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

const plugins = [];
const entry = {};

// Custom plugins
const CleanFilesPlugin = function(options) {
  if (options && !options.isArray) options = [options];
  this.options = [];
  options.forEach((o) => {
    const option = Object.assign({}, o, {
      path: o.path ? path.join(__dirname, o.path) : __dirname,
    });
    this.options.push(option);
  });
};
CleanFilesPlugin.prototype.name = 'CleanFilesPlugin';
CleanFilesPlugin.prototype.apply = (complier) => {
  const removeFiles = (option) => {
    fs.existsSync(option.path) &&
    fs.readdirSync(option.path).forEach((fileName) => {
      const filePath = option.path + '/' + fileName;
      const state = fs.statSync(filePath);
      if (option.recursive && state.isDirectory()) {
        removeFiles({
          path: filePath,
          file: option.file,
          exclude: option.exclude,
          recursive: option.recursive,
        });
      } else if (state.isFile() &&
                mm.isMatch(fileName, option.file) &&
                (option.exclude == null ||
                    !mm.isMatch(fileName, option.exclude))) {
        console.log('Delete file \x1b[36m%s\x1b[0m!', filePath);
        fs.unlinkSync(filePath);
      }
    });
  };

  complier.plugin('done', () => {
    complier.options.plugins.forEach((plugin) => {
      if (plugin.name === CleanFilesPlugin.name) {
        const options = plugin.options;
        options.forEach((option) => {
          removeFiles(option);
        });
      }
    });
  });
};

// Options for plugin
const htmlPluginOptions = {
  inject: false,
  hash: true,
  minifyOptions: {
    removeComments: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
  },
  title: packages.title,
  keyword: packages.keywords.toString(),
  description: packages.description,
};

// Functions
const loadTheme = () => {
  const templatePath = './view/template/';
  fs.readdirSync(templatePath).forEach((file) => {
    if (fs.lstatSync(templatePath + file).isFile()) {
      plugins.push(
          new HtmlWebpackPlugin(Object.assign({},
              htmlPluginOptions, {
                template: templatePath + file,
                filename: file.substring(0, file.length - 4) + '.template',
              }))
      );
    }
  });
};
loadTheme();

// Load entry
const genFilePath = (folder) => {
  return './view/' + folder + '/' + folder.upFirstChar() + '.jsx';
};
fs.readdirSync('./view').forEach((folder) => {
  if (fs.lstatSync('./view/' + folder).isDirectory() &&
        fs.existsSync(genFilePath(folder))) {
    entry[folder] = path.join(__dirname, genFilePath(folder));
  }
});

module.exports = (env, argv) => ({
  entry,
  output: {
    path: path.join(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle/[name]-[chunkhash].js',
    chunkFilename: 'bundle/[name]-[chunkhash].js',
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: {
        query: {
          plugins: [
            '@babel/syntax-dynamic-import',
            ['@babel/plugin-proposal-class-properties', {'loose': true}],
          ],
          presets: ['@babel/preset-env', '@babel/preset-react'],
        },
        loader: 'babel-loader',
      },
    }, {
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    }, {
      test: /\.css$/,
      use: ['style-loader', 'css-loader'],
    }, {
      test: /\.pug$/,
      use: 'pug-loader',
    }],
  },
  plugins: [
    ...plugins,
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new CleanFilesPlugin(argv.mode === 'production' ? [{
      path: '/public',
      file: '*.html',
      recursive: true,
    }] : [{
      path: '/public/bundle',
      file: '*.js',
      exclude: '*.min.js',
      recursive: false,
    }, {
      path: '/public',
      file: '*.template',
      recursive: true,
    }]),
    new OpenBrowserPlugin({
      url: 'http://localhost:' + (packages.port),
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    port: packages.port + 1,
    historyApiFallback: true,
  },
});
