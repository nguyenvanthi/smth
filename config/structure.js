module.exports = (app) => {
  app.structure = {
    features: {
      path: 'features',
      view: {
        path: 'features/view',
        file: '*.js',
      },
    },
    public: {
      path: 'public',
      image: {
        path: 'public/image',
      },
      bundle: {
        path: 'pbulic/bundle',
      },
    },
    resources: {
      path: 'resources',
      language: {
        path: 'resources/language',
        file: '*.json',
      },
      routes: {
        path: 'resources/routes',
        file: '*.json',
      },
    },
    services: {
      path: 'services',
      file: '*.js',
    },
    utils: {
      path: 'utils',
      file: '*.js',
    },
    view: {
      path: 'view',
      theme: {
        path: 'view/theme',
        file: '*.pug',
      },
    },
  };
};
