require('dotenv/config');
module.exports = (app, packages, root) => {
  app.utils = {}; // Functions
  app.services = {}; // SocketIO, Mail,...
  app.db = {}; // DB connection
  app.model = {}; // All model
  app.message = {}; // Info & Error message
  app.variables = {
    port: packages.port || 3000,
    title: packages.title,
    name: packages.name,
    author: packages.author,
    version: packages.version,
    dbConnectionStr: {
      firebase: packages.dbs.firebase,
    },
    root: root,
  };

  app.variables.isDebug = process.env.NODE_ENV !== 'production';
  app.variables.isDebug
    // eslint-disable-next-line no-console
    ? console.log('- Running debug mode')
    // eslint-disable-next-line no-console
    : console.log('- Running on production mode');

  app.root = root;
};
