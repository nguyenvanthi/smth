## [1.1.3] - 03/27/2019
### Quản lí nhà hàng.
  - ```Changed``` Foods.

## [1.1.2] - 03/23/2019
### Quản lí nhà hàng.
  - ```Added``` Login.

## [1.1.1] - 03/19/2019
### Quản lí nhà hàng.
  - ```Added``` Cook page.

## [1.1.0] - 03/19/2019
### Quản lí nhà hàng.
  - ```Changed``` Home API.
  - ```Removed``` Devices API, View.
  - ```Added``` Food page.

## [1.0.0] - 2/21/2019
### Init Hồ tôm
 - ```Added``` for new features.
 - ```Changed``` for changes in existing functionality.
 - ```Deprecated``` for soon-to-be removed features.
 - ```Removed``` for now removed features.
 - ```Fixed``` for any bug fixes.
 - ```Security``` in case of vulnerabilities.
