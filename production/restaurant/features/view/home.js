module.exports = (app) => {
  const Router = require('express').Router;
  const router = new Router();
  const response = app.utils.response.create('/home.template');

  [
    '/', '/index(.htm(l)?)?',
  ].forEach((route) => router.get(route, response));

  const routesPath = app.path.join(app.root,
      app.structure.resources.routes.path, 'home.json');
  const routes = require(routesPath);
  routes.forEach((route) => {
    router.get(route.link, response);
  });

  app.use('', router);
};
