/* eslint-disable no-console */
const mm = require('micromatch');
module.exports = (app) => {
  app.fs = require('fs');
  app.path = require('path');

  const loadFile = (folder, recursive = true, index = 0 ) => {
    let tab = '';
    for (let i = 0; i < index; i++) tab += '    ';
    const loadPath = app.path.join(app.variables.root, folder.path);
    let text = tab + '- ' + app.path.basename(loadPath) + ':';
    app.fs.readdirSync(loadPath).forEach((file) => {
      const filepath = app.path.join(loadPath, file);
      if (app.fs.existsSync(filepath) &&
                app.fs.statSync(filepath).isFile() &&
                mm.isMatch(file, folder.file)) {
        require(filepath)(app);
        text && (text += ' ' + file);
      }
    });
    text && console.log(text);
    if (recursive) {
      Object.keys(folder).forEach((key) => {
        if (key !== 'path' && key !== 'file') {
          loadFile(folder[key], recursive, index + 1);
        }
      });
    }
  };

  app.loadUtils = (app) => loadFile(app.structure.utils);

  app.loadFeatures = (app) => loadFile(app.structure.features);

  app.loadServices = (app) => loadFile(app.structure.services);
};
