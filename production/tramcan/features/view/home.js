module.exports = (app) => {
  const Router = require('express').Router;
  const router = new Router();
  const response = app.utils.response.create('/home.template');

  [
    '/', '/index(.htm(l)?)?',
  ].forEach((route) => router.get(route, response));

  const routesPath = app.path.join(app.root,
      app.structure.resources.routes.path, 'home.json');
  const routes = require(routesPath);
  routes.forEach((route) => {
    router.get(route.link, response);
  });

  router.post('/data', (req, res) => {
    if (req.body.id) {
      if (req.body.image) {
        if (req.body.text) {
          app.io.emit('data', req.body.id, 'all', {image: req.body.image, text: req.body.text});
        } else {
          app.io.emit('data', req.body.id, 'image', req.body.image);
        }
      } else if (req.body.text) {
        app.io.emit('data', req.body.id, 'text', req.body.text);
      } else {
        res.send({error: 'Cần phãi có Id và (image hoặc text)'});
        return;
      }
    } else {
      res.send({error: 'Cần phãi có Id và (image hoặc text)'});
      return;
    }
    res.send('OK');
  });
  app.use('', router);
};
