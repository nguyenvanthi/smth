module.exports = (app) => {
  app.utils.response = {
    getFromOtherServer: (res, serverPath) => {
      const http = require('http');
      http.get(serverPath, (response) => {
        let data = '';
        response.on('data', (chunk) => data += chunk);
        response.on('end', () => {
          res.send(data);
          let bytes = data.length / 1024 > 1024
                        ? (data.length / 1024 / 1024).toFixed(2) + ' Mb'
                        : (data.length / 1024).toFixed(2) + 'Kb';
          data.length < 1024 && (bytes = data.length + ' bytes');
          // eslint-disable-next-line no-console
          console.log('- Webpack(', bytes, ')', serverPath);
        });
      });
    },
    create: (path) => (req, res) => {
      if (app.variables.isDebug) {
        const link = 'http://localhost:' + (app.variables.port+1) + path;
        app.utils.response.getFromOtherServer(res, link);
      } else {
        const link = app.path.join(app.root, app.structure.public.path) + path;
        app.fs.readFile(link, 'utf8', (error, data) => {
          res.send(data);
        });
      }
    },
  };
};
