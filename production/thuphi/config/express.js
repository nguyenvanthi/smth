module.exports = (app) => {
  app.startServer = () => {
    const debugUrl = 'http://localhost:' + app.variables.port;
    const server = app.listen(app.variables.port,
      // eslint-disable-next-line no-console
      () => console.log('-', app.variables.name, 'listening on', debugUrl));
    app.io = require('socket.io')({ serveClient: false });
    app.io.attach(server, {
      pingInterval: 10000,
      pingTimeout: 5000,
      cookie: false
    });
    app.io.on('connection', (socket) => {
      console.log('A user connected.');
      socket.on('disconnect', () => console.log('A user disconnected'));
    });
  };

  const bodyParser = require('body-parser');
  app.use(bodyParser.json({
    limit: '50mb',
  }));
  app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
  }));

  const viewPath = app.path.join(app.root, app.structure.view.path);
  app.set('views', viewPath);
  app.set('view engine', 'pug');

  const oneYear = 356 * 24 * 60 * 60 * 1000;
  const publicPath = app.path.join(app.root, app.structure.public.path);
  app.use('/', require('express').static(publicPath, { maxAge: oneYear }));

  app.variables.isDebug === false && app.use(require('morgan')('dev'));

  // Redirect to webpack server
  if (app.variables.isDebug === true) {
    app.get('/*.js', (req, res) => {
      if (req.originalUrl.startsWith('/bundle/')) {
        app.utils.response.create(req.originalUrl)(req, res);
      } else res.next();
    });
  }

  // Favicon
  const faviconPath = app.path.join(app.root, app.structure.public.image.path);
  app.use(require('serve-favicon')(faviconPath + '/favicon.ico'));

  app.use(require('compression')());
};
